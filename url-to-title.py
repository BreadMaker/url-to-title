#!/usr/bin/env python2
# -*- coding: utf-8 -*-

# url-to-title - Obtiene el título de la URL enviada por POST al servidor
# Flask generado
#
# Versión 1.0
#
# Copyleft 2014 Felipe Peñailillo Castañeda (@breadmaker in identi.ca)
#                   <breadmaker@radiognu.org>
#               Jorge Araya Navarro (@sweet in parlementum.net)
#                   <elcorreo@deshackra.com>
#
# Este programa es software libre; puede redistribuirlo y/o modificarlo bajo
# los términos de la Licencia Pública General GNU tal como se publica por
# la Free Software Foundation; ya sea la versión 3 de la Licencia, o
# (a su elección) cualquier versión posterior.
#
# Este programa se distribuye con la esperanza de que le sea útil, pero SIN
# NINGUNA GARANTÍA; sin incluso la garantía implícita de MERCANTILIDAD o
# IDONEIDAD PARA UN PROPÓSITO PARTICULAR. Vea la Licencia Pública
# General de GNU para más detalles.
#
# Debería haber recibido una copia de la Licencia Pública General de GNU
# junto con este programa; de lo contrario escriba a la Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor,
# Boston, MA 02110-1301, EE. UU.

import sys
import os
import re
import requests
from BeautifulSoup import BeautifulSoup
import HTMLParser
from flask import Flask
from flask import request

reload(sys)
sys.setdefaultencoding('utf-8')

app = Flask(__name__)


def titulo_url(url):
    try:
        pagina = requests.get(url, stream=True)
        if pagina.status_code >= 400:
            titulo = ("Error HTTP {} al intentar acceder a"
                      " la URL.".format(pagina.status_code))
        else:
            if pagina.headers['content-type'].startswith("text/html"):
                try:
                    titulo = "[ {} ]".format(BeautifulSoup(
                        pagina.content).title.string)
                except Exception:
                    titulo = "Esa URL no tiene título, una pena."
            else:
                titulo = ("Esa no parecer ser una página web,"
                          " pero gracias por el enlace.")
    except Exception:
        return ("Ups, servicio no encontrado. O la URL está mal "
                "escrita, o no está disponible por el momento.")
    parser = HTMLParser.HTMLParser()
    return parser.unescape(' '.join(str(titulo.replace("\n", "")).split()))

@app.route('/', methods=['POST', 'GET'])
def get_title():
    if request.method == 'POST':
        url = request.form.get("url", None)
        if url is None:
            return "ola k ase enviando solicitud vacía o k ase"
        else:
            regex_url_http = re.compile(ur'((?:http|https)(?::\/{2}[\w]+)'
                '(?:[\/|\.]?)(?:[^\s"]*))', re.IGNORECASE)
            regex_url_www = re.compile(ur'((?:[a-z][a-z\.\d\-]+)\.'
                '(?:[a-z][a-z\-]+))(?![\w\.])', re.IGNORECASE)
            if regex_url_http.search(url):
                return titulo_url(regex_url_http.findall(url)[0])
            elif regex_url_www.search(url):
                return titulo_url("http://%s" % regex_url_www.findall(url)[0])
            else:
                return ("ola k ase ingresando cualquier cosa menos una"
                        " dirección HTTP o k ase")
    else:
        return "ola k ase usando método incorrecto o k ase"
    return

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(host='0.0.0.0', port=port)