# URL To Title #

Ingresas una URL, te devuelve el título de la página.

Así de simple.

## Características ##

* Funciona como una API (como, no igual a una), gracias a [Flask](http://flask.pocoo.org/).
* Compatible con [Heroku](http://url-to-title.herokuapp.com/).

## Cómo usar ##

Flask creará un servidor en [http://localhost:5000](http://localhost:5000).
Envía una solicitud via `POST` a esa dirección con cualquiera de los
siguientes datos:

```
#!json
{
    "url": "http://www.ejemplo.com?var=val&var2=val2"
}
{
    "url": "http://www.ejemplo.com"
}
{
    "url": "www.ejemplo.com"
}
{
    "url": "ejemplo.com"
}
```

y el servicio te devuelve un texto plano con el título de la página web, o un
mensaje indicando o que no hay título o que hubo otro tipo de error.